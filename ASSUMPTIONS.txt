The idea at the base of the implementation was to have frontend and backend as independent as possible, in order to improve scalability and reusability of any part of the process for any kind of pourpose. With that in mind, very basic queue communication system was decided.

This was the idea : backend webapp -> websockets -> service -> send to queue ---> listener -> websocket broadcast -> frontend webapp

the service (a facade ) would have been responsible of both sending the message to the queue and persist the data on DB.

Tech used : 
.Net MVC for the web app 
RabbitMq for the queue implementation
SignalR for websocket real-time communication with clients 
Castle Windsor as IOC Container (xml configuration)
lo4net for logging
entity framework 6 (code-first approach)
microsoft OWIN for the self hosting
newtonsoft.json for serialization/deserialization of queue messages
jquery + bootstrap on the clients.

Would have been perfect to use a MVVM client framework like angularJs, but i just started to use and know it and that would have taken too much time. 
I had little time to work on this but did not want to miss the deadline, so i decided in this case to stick with something uglier but more secure.

In the solution backend and frontend are in the same webapp, but they use 2 different hubs to communicate and/or receive information. That was done specifically, in order to let the backend continue to grow in complexity without giving to the broadcaster more burden.
In fact, the real-time communication between the backend operators is implemented internally in the webapp itself, while the communication with the clients is made trough a separated queue system, that is also a self-hosted signalR broadcaster. 
This will allow the system to respond easily to increments in complexity and in traffic consumption. There will be the possibility of hosting the backend on a little server, while the broadcast signalR application (that could be easily transformed in a windows service) can be hosted on several machine and evolve indipendently.
With the same concept in mind came the use of Castle Windsor as a IOC Container, that worked wonderfully in resolving dependencies both for the mvc controllers and for the signalR hubs. 

In this way was easier to separate the model from the presentation of the data. In a real trading tool, usually the data is arranged differently between the frontend (website, f.e.) end the backend. With this solution, it will be possible to upgrade the hubs (using client groups, for example, adding connected or diconnected events, ecc.) in totally different ways for the two system. The same for the queue, that could call different kind of broadcasters, or adding another layer of caching, or just logging.

At the moment only one implementation for each interface was needed, but a heavy use of generics was decided anyway (specifically the generic repository pattern) in the bll and dal layer of the application. This will allow those interfaces to serve different purposes during the development and save a lot of time when new feature will be added. The use of singletons was avoided for the same reason - having less code as possible involved in every level of the solution.
The database schema has been created in order to not lose the data that has just been updated, but to keep it saved. Every Odd will mantain all the values it ever add. 

The solution was created in a way to decrease as much as possible database access. Right now the clients - both frontend and backend - will access the database (or a cache, in the real world) only when they arrive for the first time, but they will be updated in real time by the broadcast(frontend) or by the app itself(backend).
Another issue that presented to me was the concurrency problem in case of more users editing the same event. The problem was partially solved by asking users to lock the event they are working on, and having this lock propagate in real time to other backend clients that would be unable to edit the same row. Of course, this solution is not final because it's not enforced on the server, but it's a start.

The login is the basic login that comes with the MVC template - it was ok for what was asked and it's very simple and straightforward. Other parts of the solution required more focus, so it stayed.

The solution can be greatly improved: first of all, it lacks validation, error handling, and logging, at every level (and has several small bugs). The total lack of Unit Testing is a sin for wich I will stay in hell forever. Also, probably .net MVC was not the best technology for this situation : as said, something like knockout or angular (a single page application) would have been perfect. In fact, working on this project and on this requirement had made me realize the incredible potential of that + signalR. The inheritance can be used more in order to improve the solution (between the two hubs, for example, and in javascript where is totally absent - there is a lot of duplication in the js code).

Please let me know your toughts. 
Thank you very much,
Nicola Atorino