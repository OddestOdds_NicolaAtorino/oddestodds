# README #

In order for this solution to work, RabbitMq server has to be installed on the machine.
It is possible to download it at https://www.rabbitmq.com/download.html .

Solution is divided in 3 projects. Running the solution from within Visual Studio will trigger the execution of both the webapplication and the listener/broadcaster console app. 

Without the listener, only the backend will work.

In order for the listener to work properly, localhost:8080 has to be free. The port can be changed easily from the code, anyway.

Unfortunately, the code has no tests to run. It is also error-prone, probably still bugged here and there and contains several 'not ideal' solutions. 

This was also for me a beautiful way of trying new things and see what i could do. If you find something that you don't like (and you probably will!) please let me know, I look forward in speaking about the implementation I did and also telling you what i would like to improve in it (several things).

You can contact me anytime you like for additional informations or explanation.

Kindest Regards, Nicola Atorino