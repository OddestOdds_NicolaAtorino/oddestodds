﻿using Microsoft.Owin.Hosting;
using OddestOdds.Common.BLL.Hubs;
using OddestOdds.Common.BLL.Entities;
using OddestOdds.Common.BLL.Queue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddestOdds.Listener
{
    /* this is a simple console application that host signalR and listens for RabbitMq Messages, 
     * but it will be a windows service  in an eventual full product. 
     * Anyway, for now it serves is purpose. */
    class Program
    {
        static void Main(string[] args)
        {
            string url = "http://localhost:8080";
            using (WebApp.Start(url))
            {
                var hub = new OddestOddsHub();
                Console.WriteLine("SignalR Server running on {0}...", url);
                Console.WriteLine("RabbitMq listener is listening....");
                while (true)
                {
                    var queue = new GameQueue();
                    queue.OnReceived(x =>
                    {
                        hub.BroadCast(x);
                        Console.WriteLine(string.Format("{0} event {1} Sent to hub", x.OperationType, x.OperationSubject.Name));
                    });
                }
            }
        }
    }
}
