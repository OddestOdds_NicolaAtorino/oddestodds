﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OddestOdds.Presentation.Models
{
    public class GameViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int HomeOddId { get; set; }

        public double HomeOddValue { get; set; }

        public int DrawOddId { get; set; }

        public double DrawOddValue { get; set; }

        public int AwayOddId { get; set; }

        public double AwayOddValue { get; set; }

    }
}