﻿$(function () {

    //this is hosted by the webApp
    //$.connection.hub.url = "http://localhost:8080/signalr";
    $.connection.hub.logging = true;
    var oddestOdds = $.connection.oddestOddsBackendHub;

    oddestOdds.client.createGame = function (game) {
        //we will create another row with the new event.
        //a good way for doing this on a finished product
        //will be with a specific library, like moustache.
        var $clientGame = $('.row[data-eventid="' + game.Id + '"]');

        if (!$clientGame.length) {

            var $gameContainer = $("#gameContainer");

            var html =
                '<div class="row game" data-eventid="' + game.Id + '">'
                + '<div class="col-xs-1 delete">'
                  + '<span class="txt-locked" style="color:red;display:none;">Locked!</span>'
                  + '<button class="btn btn-sm btn-delete">Delete</button>'
                + '</div>'
                + '<div class="col-xs-5 name">' + game.Name + '</div>'
                + '<div class="col-xs-1 odd-home" data-oddid="' + game.HomeOddId + '">'
                  + '<input type="text" class="txtHomeOdd" value="' + game.HomeOddValue + '" disabled />'
                + '</div>'
                + '<div class="col-xs-1 odd-draw" data-oddid="' + game.DrawOddId + '">'
                  + '<input type="text" class="txtHomeOdd" value="' + game.DrawOddValue + '" disabled />'
                + '</div>'
                + '<div class="col-xs-1 odd-away" data-oddid="' + game.AwayOddId + '">'
                  + '<input type="text" class="txtHomeOdd" value="' + game.AwayOddValue + '" disabled />'
                + '</div>'
                + '<div class="col-xs-3 commands">'
                  + '<button class="btn btn-sm btn-lock">Lock</button>'
                  + '<button class="btn btn-sm btn-update" style="display:none;">Update</button>'
                + '</div>';

            var $gameDiv = $(html);
            $gameContainer.append($gameDiv);
        }
    };

    oddestOdds.client.deleteGame = function (gameId) {
        var $clientGame = $('.row[data-eventid="' + gameId + '"]');
        if ($clientGame.length) {
            $clientGame.remove();
        }
    };

    oddestOdds.client.updateGame = function (game) {
        $('.odd-home[data-oddid=' + game.HomeOddId + '] > input').val(game.HomeOddValue)
        $('.odd-draw[data-oddid=' + game.DrawOddId + '] > input').val(game.DrawOddValue)
        $('.odd-away[data-oddid=' + game.AwayOddId + '] > input').val(game.AwayOddValue)
    };

    oddestOdds.client.lockGame = function (gameId) {
        var $clientGame = $('.row[data-eventid="' + gameId + '"]');
        if ($clientGame.length) {   //check if exists

            $clientGame.find('.btn-delete').hide();
            $clientGame.find('.txt-locked').show();

            $clientGame.find('.btn-lock,.btn-update').hide();
        }
    };

    oddestOdds.client.unlockGame = function (gameId) {
        var $clientGame = $('.row[data-eventid="' + gameId + '"]');
        if ($clientGame.length) {   //check if exists

            $clientGame.find('.btn-delete').show();
            $clientGame.find('.txt-locked').hide();
            $clientGame.find('.btn-lock').show();
        }
    };

    //this locks the row. We use delegated event in order to attach
    //the handler also to dom elements added dinamically.
    $.connection.hub.start().done(function () {

        $('#gameContainer').on('click', '.btn-lock', function (e) {
            var $context = GetContext(this);

            //check if the row is locked
            var locked = $(this).text() == 'Unlock'

            if (locked) {
                //hub
                oddestOdds.server.unlock($context.data('eventid'));

                //mask
                $(this).text('Lock');
                $context.find('.btn-update').hide();
                $context.find('input[type=text]').prop('disabled', true);
            } else {
                //hub
                oddestOdds.server.lock($context.data('eventid'));

                //mask
                $(this).text('Unlock');
                $context.find('.btn-update').show();
                $context.find('input[type=text]').prop('disabled', false);
            }
        });

        $('#gameContainer').on('click', '.btn-update', function (e) {
            //info
            var $context = GetContext(this);
            var game = getGameFromContext($context);
            //hub
            oddestOdds.server.update(game);


            $(this).hide();
            $context.find('.btn-lock').text('Lock');
            $context.find('input[type=text]').prop('disabled', true);
        });

        $('#gameContainer').on('click', '.btn-delete', function (e) {
            //info
            var $context = GetContext(this);
            //hub
            oddestOdds.server.delete($context.data('eventid'));

            $context.remove();
        });

        $('.btn-create').on('click', function (e) {
            var game = {
                Id: 0,
                Name: $('#txtNewEvent').val(),
                HomeOddValue: $('#txtNewHomeOdd').val(),
                DrawOddValue: $('#txtNewDrawOdd').val(),
                AwayOddValue: $('#txtNewAwayOdd').val(),

            };
            //hub
            oddestOdds.server.create(game);

            $('#txtNewEvent').val('');
            $('#txtNewHomeOdd').val('');
            $('#txtNewDrawOdd').val('');
            $('#txtNewAwayOdd').val('');
        });

        alert('Backend Hub Connection is up');
    });


    function GetContext(elem) {
        return $(elem).parents('.game');
    }

    function getGameFromContext(context) {
        var eventId = context.data("eventid");
        var eventName = context.contents(".name").text();   //.contents() , because .find() does not return text nodes.

        var homeOddId = context.find(".odd-home").data('oddid');
        var drawOddId = context.find(".odd-draw").data('oddid');
        var awayOddId = context.find(".odd-away").data('oddid');

        var homeOddVal = context.find(".odd-home > input").val();
        var drawOddVal = context.find(".odd-draw > input").val();
        var awayOddVal = context.find(".odd-away > input").val();

        return {
            Id: eventId,
            Name: eventName,
            HomeOddId: homeOddId,
            DrawOddId: drawOddId,
            AwayOddId: awayOddId,
            HomeOddValue: homeOddVal,
            DrawOddValue: drawOddVal,
            AwayOddValue: awayOddVal
        };
    };
});