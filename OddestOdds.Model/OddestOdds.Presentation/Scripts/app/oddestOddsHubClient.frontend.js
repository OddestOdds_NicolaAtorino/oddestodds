﻿$(function () {
    $.connection.hub.url = "http://localhost:8080/signalr";
    var oddestOdds = $.connection.oddestOddsHub;
    oddestOdds.client.create = function (game) {
        //we will create another row with the new event.
        //a good way for doing this on a finished product
        //will be with a specific library, like moustache.
        var clientGame = $('.row[data-eventid="' + game.EventId + '"]');

        if (clientGame[0] == undefined) {

            var $gameContainer = $("#gameContainer");

            var homeOdd = $.grep(game.GameOdds, function (odd) { return odd.Name == '1'; })[0];
            var drawOdd = $.grep(game.GameOdds, function (odd) { return odd.Name == 'X'; })[0];
            var awayOdd = $.grep(game.GameOdds, function (odd) { return odd.Name == '2'; })[0];

            var html =
              '<div class="row" data-eventid="' + game.EventId + '"><div class="col-xs-6">' + game.Name + '</div>'
            + '<div class="col-xs-2 odd" data-oddid="' + homeOdd.Id + '">' + homeOdd.Value + '</div>'
            + '<div class="col-xs-2 odd" data-oddid="' + drawOdd.Id + '">' + drawOdd.Value + '</div>'
            + '<div class="col-xs-2 odd" data-oddid="' + awayOdd.Id + '">' + awayOdd.Value + '</div>'
            + '</div>';

            var $gameDiv = $(html);
            $gameContainer.append($gameDiv);
        }
    };
    oddestOdds.client.delete = function (game) {
        var clientGame = $('.row[data-eventid="' + game.EventId + '"]');
        if (clientGame) {
            clientGame.remove();
        }
    };
    oddestOdds.client.update = function (game) {
        for (var i = 0; i < game.GameOdds.length; i++) {
            var clientOdd = $('.odd[data-oddid="' + game.GameOdds[i].Id + '"]');
            clientOdd.text(game.GameOdds[i].Value);
        }
    };
    $.connection.hub.start().done(function () {
        alert('Connection is up');
    });
});