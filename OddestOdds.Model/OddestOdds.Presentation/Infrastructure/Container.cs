﻿using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.Windsor;
using Castle.Windsor.Configuration.Interpreters;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OddestOdds.Presentation.Infrastructure
{
    public class CustomContainer
    {
        private static ILog _logger = LogManager.GetLogger(typeof(CustomContainer));

        private static readonly Lazy<CustomContainer> _instance = new Lazy<CustomContainer>(() =>
        {
            _logger.Debug("Start Custom Container Init");
            var c = new CustomContainer();
            c.Setup();
            _logger.Debug("End Custom Container Init");
            return c;
        });

        public IWindsorContainer Container { get; private set; }

        public static CustomContainer Instance { get { return _instance.Value; } }

        private void Setup()
        {
            try
            {
                Container = new WindsorContainer(new XmlInterpreter());
                Container.Kernel.Resolver.AddSubResolver(
                new CollectionResolver(Container.Kernel));
                //Container.AddFacility<WcfFacility>();
            }
            catch (Exception ex)
            {
                _logger.Error("Custom Container Setup Error : ", ex);
                throw ex;
            }
        }
    }

}