﻿using OddestOdds.Common.BLL.Entities;
using OddestOdds.Common.BLL.Queue;
using OddestOdds.Common.BLL.Services;
using OddestOdds.Common.DAL.Entities;
using OddestOdds.Presentation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OddestOdds.Presentation.Controllers
{
    public class HomeController : Controller
    {
        private IGameService _gameService;


        public HomeController(IGameService gameService)
        {
            _gameService = gameService;
        }

        public ActionResult Index()
        {
            var games = _gameService.GetAllGames();
            var model = games.Select(s => new GameViewModel()
            {

                Id = s.EventId,
                Name = s.Name,
                HomeOddId = s.GameOdds.Single(x => x.Name == "1").Id,
                HomeOddValue = s.GameOdds.Single(x => x.Name == "1").Value,

                DrawOddId = s.GameOdds.Single(x => x.Name == "X").Id,
                DrawOddValue = s.GameOdds.Single(x => x.Name == "X").Value,

                AwayOddId = s.GameOdds.Single(x => x.Name == "2").Id,
                AwayOddValue = s.GameOdds.Single(x => x.Name == "2").Value
            });
            return View(model);
        }

        [Authorize]
        public ActionResult Backend(string id)
        {
            var games = _gameService.GetAllGames();
            var model = games.Select(s => new GameViewModel()
            {
                Id = s.EventId,
                Name = s.Name,
                HomeOddId = s.GameOdds.Single(x => x.Name == "1").Id,
                HomeOddValue = s.GameOdds.Single(x => x.Name == "1").Value,

                DrawOddId = s.GameOdds.Single(x => x.Name == "X").Id,
                DrawOddValue = s.GameOdds.Single(x => x.Name == "X").Value,

                AwayOddId = s.GameOdds.Single(x => x.Name == "2").Id,
                AwayOddValue = s.GameOdds.Single(x => x.Name == "2").Value
            });
            return View(model);
        }

        public ActionResult Contact()
        {
            return View();
        }
    }
}