﻿using Microsoft.AspNet.SignalR;
using OddestOdds.Common.BLL.BroadCasters;
using OddestOdds.Common.BLL.Entities;
using OddestOdds.Common.BLL.Hubs;
using OddestOdds.Common.BLL.Services;
using OddestOdds.Presentation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OddestOdds.Presentation.Hubs
{
    public class OddestOddsBackendHub : Hub
    {
        private IGameService _gameService;

        public OddestOddsBackendHub(IGameService gameService)
        {
            _gameService = gameService;
        }

        public void Create(GameViewModel model)
        {
            var game = new Game()
            {
                EventId = 0,
                Name = model.Name,
                GameOdds = new List<GameOdd>()
                {
                    new GameOdd(){ Name =  "1",  Value = model.HomeOddValue },
                    new GameOdd(){ Name =  "X",  Value = model.DrawOddValue },
                    new GameOdd(){ Name =  "2",  Value = model.AwayOddValue },
                }
            };
            model.Id = _gameService.CreateGame(game);

            //now we will add the OddIds to the model and send it back to the future
            var createdGame = _gameService.Find(model.Id);

            model.HomeOddId = createdGame.GameOdds.Single(x => x.Name == "1").Id;
            model.DrawOddId = createdGame.GameOdds.Single(x => x.Name == "X").Id;
            model.AwayOddId = createdGame.GameOdds.Single(x => x.Name == "2").Id;

            Clients.All.createGame(model);
        }

        public void Update(GameViewModel model)
        {
            var game = new Game()
            {
                EventId = model.Id,
                Name = model.Name,
                GameOdds = new List<GameOdd>()
                {
                    new GameOdd(){ Name =  "1", Id=model.HomeOddId,  Value = model.HomeOddValue },
                    new GameOdd(){ Name =  "X", Id=model.DrawOddId,  Value = model.DrawOddValue },
                    new GameOdd(){ Name =  "2", Id=model.AwayOddId,  Value = model.AwayOddValue },
                }
            };

            _gameService.UpdateGame(game);

            Clients.Others.updateGame(model);
            System.Threading.Thread.Sleep(100);
            Clients.Others.UnlockGame(model.Id);
        }

        public void Delete(int gameId)
        {
            _gameService.DeleteGame(gameId);
            
            Clients.Others.deleteGame(gameId);
        }

        public void Lock(int gameId)
        {
            Clients.Others.LockGame(gameId);
        }

        public void Unlock(int gameId)
        {
            Clients.Others.UnlockGame(gameId);
        }
    }
}