﻿using Castle.Windsor;
using Castle.Windsor.Installer;
using OddestOdds.Common.DAL.Context;
using OddestOdds.Presentation.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace OddestOdds.Presentation
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);


            //D.I. initialization. Instance call will trigger the lazy loading.
            //var useless = CustomContainer.Instance;
            
            

            /*
             * run this once to populate the DB
             * 
            
             */
            Database.SetInitializer(new OddestOddsInitializer());
            var c = new OddestOddsContext();
            c.Database.Initialize(true);
        }
    }
}
