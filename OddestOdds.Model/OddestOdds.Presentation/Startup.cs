﻿using Castle.Windsor;
using Castle.Windsor.Installer;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using OddestOdds.Presentation.Infrastructure;
using Owin;
using System.Web.Mvc;

[assembly: OwinStartupAttribute(typeof(OddestOdds.Presentation.Startup))]
namespace OddestOdds.Presentation
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var container = new WindsorContainer().Install(FromAssembly.This());
            var controllerFactory = new WindsorControllerFactory(container.Kernel);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);

            var signalRDependencyResolver = new SignalRResolver(container);
            var config = new HubConfiguration();
            config.Resolver = signalRDependencyResolver;
            app.MapSignalR(config);

            ConfigureAuth(app);
        }
    }
}
