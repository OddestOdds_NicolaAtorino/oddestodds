﻿using OddestOdds.Common.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddestOdds.Common.DAL.Context
{
    public class OddestOddsInitializer : DropCreateDatabaseAlways<OddestOddsContext>
    {
        protected override void Seed(OddestOddsContext context)
        {
            var events = new List<Event>{
                new Event { Name= "Manchester-Liverpool" },
                new Event { Name= "AS Roma - SS-Lazio" },
                new Event { Name= "Paris Saint German - Lione" },
                new Event { Name= "Real-Liverpool" },

            };
            context.Events.AddRange(events);
            context.SaveChanges();

            var marketTypes = new List<MarketType>{
                new MarketType { Name="1X2", ChoiceNumber=3 }
            };
            context.MarketTypes.AddRange(marketTypes);
            context.SaveChanges();

            var markets = new List<Market>{
                new Market{ EventId=1, MarketTypeId = 1 },
                new Market{ EventId=2, MarketTypeId = 1 },
                new Market{ EventId=3, MarketTypeId = 1 },
                new Market{ EventId=4, MarketTypeId = 1 },
            };
            context.Markets.AddRange(markets);
            context.SaveChanges();

            var odds = new List<Odd>
            {
                new Odd{ MarketId = 1, Name = "1", SortOrder = 1},
                new Odd{ MarketId = 1, Name = "X", SortOrder = 2},
                new Odd{ MarketId = 1, Name = "2", SortOrder = 3},

                new Odd{ MarketId = 2, Name = "1", SortOrder = 1},
                new Odd{ MarketId = 2, Name = "X", SortOrder = 2},
                new Odd{ MarketId = 2, Name = "2", SortOrder = 3},

                new Odd{ MarketId = 3, Name = "1", SortOrder = 1},
                new Odd{ MarketId = 3, Name = "X", SortOrder = 2},
                new Odd{ MarketId = 3, Name = "2", SortOrder = 3},

                new Odd{ MarketId = 4, Name = "1", SortOrder = 1},
                new Odd{ MarketId = 4, Name = "X", SortOrder = 2},
                new Odd{ MarketId = 4, Name = "2", SortOrder = 3},
            };
            context.Odds.AddRange(odds);
            context.SaveChanges();

            var creationTime = DateTime.Now;
            var random = new Random(12345);
            random.NextDouble();
            var oddValues = new List<OddValue>
            {
                new OddValue{ OddId = 1, Value= Math.Round(1 + random.NextDouble(),3), CreationTime = creationTime},
                new OddValue{ OddId = 2, Value=Math.Round(1 + random.NextDouble(),3), CreationTime = creationTime},
                new OddValue{ OddId = 3, Value=Math.Round(1 + random.NextDouble(),3), CreationTime = creationTime},

                new OddValue{ OddId = 4, Value=Math.Round(1 + random.NextDouble(),3), CreationTime = creationTime},
                new OddValue{ OddId = 5, Value=Math.Round(1 + random.NextDouble(),3), CreationTime = creationTime},
                new OddValue{ OddId = 6, Value=Math.Round(1 + random.NextDouble(),3), CreationTime = creationTime},
                
                new OddValue{ OddId = 7, Value=Math.Round(1 + random.NextDouble(),3), CreationTime = creationTime},
                new OddValue{ OddId = 8, Value=Math.Round(1 + random.NextDouble(),3), CreationTime = creationTime},
                new OddValue{ OddId = 9, Value=Math.Round(1 + random.NextDouble(),3), CreationTime = creationTime},
                
                new OddValue{ OddId = 10, Value=Math.Round(1 + random.NextDouble(),3), CreationTime = creationTime},
                new OddValue{ OddId = 11, Value=Math.Round(1 + random.NextDouble(),3), CreationTime = creationTime},
                new OddValue{ OddId = 12, Value=Math.Round(1 + random.NextDouble(),3), CreationTime = creationTime}
            };
            context.OddValues.AddRange(oddValues);
            context.SaveChanges();


        }
    }
}
