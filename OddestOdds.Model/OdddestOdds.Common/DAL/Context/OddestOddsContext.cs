﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OddestOdds.Common.DAL.Entities;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace OddestOdds.Common.DAL.Context
{
    public class OddestOddsContext : DbContext
    {
        public OddestOddsContext() : base("OddestOddsContext")
        {
        }

        public DbSet<Event> Events { get; set; }
        public DbSet<Market> Markets { get; set; }
        public DbSet<MarketType> MarketTypes { get; set; }
        public DbSet<Odd> Odds { get; set; }
        public DbSet<OddValue> OddValues { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
