﻿using OddestOdds.Common.DAL.Context;
using System;
namespace OddestOdds.Common.DAL.Generics
{
    public interface IGenericRepository<T>
     where T : class
    {
        
        void Add(T entity);
        void Delete(T entity);
        void Edit(T entity);
        System.Linq.IQueryable<T> FindBy(System.Linq.Expressions.Expression<Func<T, bool>> filter);
        System.Linq.IQueryable<T> GetAll();
        void Save();
    }
}
