﻿using OddestOdds.Common.DAL.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddestOdds.Common.DAL.Generics
{
    /*this repository can be used as generic for all the repository 
     * I should need in the application. I would have liked to use an interface
     * also for the base DbContextClass, but it does not seems to exists*/
    public abstract class GenericRepository<T> :  IGenericRepository<T> 
        where T : class 
    {
        //Having this protected allow subclasses to eventually 
        //do complex work not needed in the base class. (joins etc).
        protected OddestOddsContext Context = new OddestOddsContext();

        public IQueryable<T> GetAll() {
            return Context.Set<T>();
        }

        public IQueryable<T> FindBy(
            System.Linq.Expressions.Expression<Func<T,bool>> filter)
        {
            return Context.Set<T>().Where(filter);
        }

        public virtual void Add(T entity)
        {
            Context.Set<T>().Add(entity);
        }

        public virtual void Delete(T entity)
        {
            Context.Set<T>().Remove(entity);
        }

        public virtual void Edit(T entity)
        {
            Context.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public virtual void Save()
        {
            Context.SaveChanges();
        }

    }
}
