﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddestOdds.Common.DAL.Entities
{
    public class Odd
    {
        public int Id { get; set; }
        public int MarketId { get; set; }
        public virtual List<OddValue> OddValue { get; set; }
        public int SortOrder { get; set; }
        public string Name { get; set; }
    }

}
