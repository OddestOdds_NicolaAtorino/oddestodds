﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddestOdds.Common.DAL.Entities
{
    public class MarketType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ChoiceNumber { get; set; }
        public virtual ICollection<Market> Markets { get; set; }
    }
}
