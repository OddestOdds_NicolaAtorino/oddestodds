﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddestOdds.Common.DAL.Entities
{
    public class Market
    {
        public int Id { get; set; }
        public int EventId { get; set; }
        public int MarketTypeId { get; set; }
        public virtual ICollection<Odd> Odds { get; set; }
    }

}
