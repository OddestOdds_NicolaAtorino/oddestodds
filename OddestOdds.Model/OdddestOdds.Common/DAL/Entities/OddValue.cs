﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddestOdds.Common.DAL.Entities
{
    //oddvalue is separated so it's easy to create a system that will mantain the odd's value fluctuation over time.
    public class OddValue 
    {
        public int Id { get; set; }
        public int OddId { get; set; }
        public double Value { get; set; }
        public DateTime CreationTime { get; set; }
    }

}
