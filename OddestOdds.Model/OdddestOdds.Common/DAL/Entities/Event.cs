﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddestOdds.Common.DAL.Entities
{
    public class Event
    {
        public int Id { get; set; }
        public string Name { get; set; }

        //market collection. For this demo, we will use only one market, 1x2.
        public virtual ICollection<Market> Markets { get; set; } 
    }
}
