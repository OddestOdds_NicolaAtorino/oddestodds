﻿using OddestOdds.Common.DAL.Generics;
using OddestOdds.Common.DAL.Entities;
using OddestOdds.Common.DAL.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddestOdds.Common.DAL.Repositories
{
    public interface IEventRepository : IGenericRepository<Event>
    {

    }
    public class EventRepository :  GenericRepository<Event>, IEventRepository
    {
    }
}
