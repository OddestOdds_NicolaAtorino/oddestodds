﻿using Microsoft.AspNet.SignalR;
using OddestOdds.Common.BLL;
using OddestOdds.Common.BLL.Entities;
using OddestOdds.Common.BLL.Queue;
using OddestOdds.Common.BLL.BroadCasters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddestOdds.Common.BLL.Hubs
{
    public class OddestOddsHub : Hub
    {
        private readonly GameOperationBroadcaster _operationBroadcaster;

        public OddestOddsHub() : this(GameOperationBroadcaster.Instance) { }

        public OddestOddsHub(GameOperationBroadcaster operationBroadcaster)
        {
            _operationBroadcaster = operationBroadcaster;
        }

        public void BroadCast(Operation<Game> operation)
        {
            _operationBroadcaster.BroadcastOperation(operation);
        }
    }
}
