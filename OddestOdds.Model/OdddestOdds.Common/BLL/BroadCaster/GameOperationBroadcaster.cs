﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using OddestOdds.Common.BLL.Entities;
using OddestOdds.Common.BLL.Queue;
using OddestOdds.Common.BLL.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddestOdds.Common.BLL.BroadCasters
{
    public class GameOperationBroadcaster : OperationBroadcaster<Game>
    {
        private readonly static Lazy<GameOperationBroadcaster> _instance = 
            new Lazy<GameOperationBroadcaster>(() => new GameOperationBroadcaster(GlobalHost.ConnectionManager.GetHubContext<OddestOddsHub>().Clients));

        private GameOperationBroadcaster(IHubConnectionContext<dynamic> clients)
        {
            Clients = clients;
        }

        public void BroadcastOperation(Operation<Game> operation)
        {
            base.BroadcastOperation(operation);
        }

        public static GameOperationBroadcaster Instance
        {
            get
            {
                return _instance.Value;
            }
        }
    }
}
