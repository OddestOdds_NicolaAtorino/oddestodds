﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using OddestOdds.Common.BLL.Queue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddestOdds.Common.BLL.BroadCasters
{
    public abstract class OperationBroadcaster<T>
    {
        protected IHubConnectionContext<dynamic> Clients
        {
            get;
            set;
        }
        protected virtual void BroadcastOperation(IOperation<T> operation)
        {
            switch (operation.OperationType)
            {
                case OperationType.Create:
                    BroadcastCreation<T>(operation.OperationSubject);
                    break;
                case OperationType.Delete:
                    BroadcastDeletion(operation.OperationSubject);
                    break;
                case OperationType.Update:
                    BroadcastUpdate(operation.OperationSubject);
                    break;
                default:
                    throw new NotImplementedException(string.Format("Operation type {0} not implemented", operation.OperationType));
            }
        }

        private void BroadcastCreation<T>(T subject)
        {
            Clients.All.create(subject);
        }

        private void BroadcastDeletion<T>(T subject)
        {
            Clients.All.delete(subject);
        }

        private void BroadcastLock<T>(T subject)
        {
            Clients.All.lockForChanges(subject);
        }

        private void BroadcastUpdate<T>(T subject)
        {
            Clients.All.update(subject);
        }

        private void BroadcastUnlock<T>(T subject)
        {
            Clients.All.unlockForChanges(subject);
        }

    }
}
