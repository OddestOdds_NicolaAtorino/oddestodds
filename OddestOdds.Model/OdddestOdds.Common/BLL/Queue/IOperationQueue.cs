﻿using System;
namespace OddestOdds.Common.BLL.Queue
{
    public interface IOperationQueue<T,V>
     where T : IOperation<V>
    {
        void OnReceived(Action<T> action);
        void Send(T message);
    }
}
