﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.MessagePatterns;
using System;
using System.Text;

namespace OddestOdds.Common.BLL.Queue
{
    public abstract class OperationQueue<T, V> : IOperationQueue<T, V>
        where T : IOperation<V>
        where V : new()
    {
        private IConnectionFactory _connFactory;
        private string _queue = typeof(T).ToString();

        public OperationQueue()
        {
            _connFactory = new ConnectionFactory() { HostName = "localhost" };  //connect to local machine for demo purpose. If I have time will manage this with D.I. named Instances.
        }

        private void DeclareQueue(IModel model)
        {
            //this is an idempotent operation that prevents client crashes if the queue does not exists.
            //the queue qill have the same name of the operation it is pushing.
            model.QueueDeclare(
                queue: _queue,
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null);
        }

        public virtual void OnReceived(Action<T> action)
        {
            using (var conn = _connFactory.CreateConnection())
            using (var model = conn.CreateModel())
            {
                DeclareQueue(model);
                var subscription = new Subscription(model, _queue, false);
                BasicDeliverEventArgs basicDeliveryEventArgs = subscription.Next(); //stops and listens
                var message = Encoding.UTF8.GetString(basicDeliveryEventArgs.Body);
                var viewModel = JsonConvert.DeserializeObject(message, Type.GetType(typeof(T).AssemblyQualifiedName));
                var notGen = (T)viewModel;

                Console.WriteLine(" [x] Received {0}", message);
                action.Invoke(notGen);
                subscription.Ack(basicDeliveryEventArgs);
            }
        }

        public virtual void Send(T message)
        {
            using (var conn = _connFactory.CreateConnection())
            using (var model = conn.CreateModel())
            {
                DeclareQueue(model);
                byte[] body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(message));
                model.BasicPublish(exchange: "", routingKey: _queue, basicProperties: null, body: body);
            }
        }


    }
}
