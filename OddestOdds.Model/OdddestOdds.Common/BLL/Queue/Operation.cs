﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OddestOdds.Common.BLL.Queue
{
    public class Operation<T> : IOperation<T>
    {
        public Operation(OperationType type, T subject)
        {
            OperationType = type;
            OperationSubject = subject;
        }
        public OperationType OperationType {get; set;}
        public T OperationSubject {get; set;}

        public int CallerId {get; set;}
    }
}
