﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Text;
using RabbitMQ.Client.Events;
using System.Threading.Tasks;
using OddestOdds.Common.DAL.Entities;
using RabbitMQ.Client.MessagePatterns;
using OddestOdds.Common.BLL.Entities;

namespace OddestOdds.Common.BLL.Queue
{
    public interface IGameQueue : IOperationQueue<Operation<Game>,Game>
    {

    }

    /*very simple implementation of send/receive methods*/
    public class GameQueue : OperationQueue<Operation<Game>,Game>, IGameQueue
    {
        
    }
}