﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddestOdds.Common.BLL.Queue
{
    public enum OperationType
    {
        Create,
        Delete,
        Update,
    }

    public interface IOperation<T>
    {
        OperationType OperationType { get; set; }

        int CallerId { get; set; }

        T OperationSubject { get; set; }
    }
}
