﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OddestOdds.Common.BLL.Entities
{
    public class GameOdd
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Value { get; set; }
    }

}
