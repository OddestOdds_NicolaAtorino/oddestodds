﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddestOdds.Common.BLL.Entities
{
    public class Game
    {
        public int EventId { get; set; }
        public string Name { get; set; }
        public IEnumerable<GameOdd> GameOdds { get; set; }

    }

   

}
