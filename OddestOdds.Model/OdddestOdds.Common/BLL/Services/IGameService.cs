﻿using OddestOdds.Common.BLL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddestOdds.Common.BLL.Services
{
    //provides several operations available to the 'Game' business object
    public interface IGameService
    {
        int CreateGame(Game game);
        void DeleteGame(int gameId);
        void UpdateGame(Game game);
        IEnumerable<Game> GetAllGames();

        Game Find(int gameId);

    }
}
