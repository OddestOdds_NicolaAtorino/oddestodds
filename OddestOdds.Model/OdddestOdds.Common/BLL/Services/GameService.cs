﻿using OddestOdds.Common.BLL.Entities;
using OddestOdds.Common.BLL.Queue;
using OddestOdds.Common.DAL.Entities;
using OddestOdds.Common.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddestOdds.Common.BLL.Services
{
    public class GameService : IGameService
    {
        private IEventRepository _eventRepository;
        private IMarketTypeRepository _marketTypeRepository;
        private IGameQueue _gameQueue;

        //this simple aggregator class is responsible of calling the repository 
        //for persistence and for calling the queue 'send' method, in order to start the message chain.
        public GameService(IEventRepository eventRepository, IMarketTypeRepository marketTypeRepository, IGameQueue gameQueue)
        {
            _eventRepository = eventRepository;
            _marketTypeRepository = marketTypeRepository;
            _gameQueue = gameQueue;
        }

        public int CreateGame(Game game)
        {
            var creationTime = DateTime.Now;

            //entity creation - odd values
            var oddvalue_1 = new OddValue()
            {
                CreationTime = creationTime,
                Value = game.GameOdds.Single(x => x.Name == "1").Value
            };
            var oddvalue_X = new OddValue()
            {
                CreationTime = creationTime,
                Value = game.GameOdds.Single(x => x.Name == "X").Value
            };
            var oddvalue_2 = new OddValue()
            {
                CreationTime = creationTime,
                Value = game.GameOdds.Single(x => x.Name == "2").Value
            };

            //entity creation - odds
            var odds = new List<Odd>()
            {
                new Odd()
                {
                    Name = "1", SortOrder=1, OddValue = new List<OddValue>() { oddvalue_1}
                },
                new Odd()
                {
                    Name = "X", SortOrder=2, OddValue = new List<OddValue>() { oddvalue_X}
                },
                new Odd()
                {
                    Name = "2", SortOrder=2, OddValue = new List<OddValue>() { oddvalue_2}
                },
            };

            //entity creation - Market
            var markets = new List<Market>{
                new Market(){
                    MarketTypeId = _marketTypeRepository.FindBy(x => x.Name == "1X2").First().Id,
                    Odds  = odds
                }
            };

            //entity creation - Event
            var @event = new Event()
            {
                Markets = markets,
                Name = game.Name
            };

            _eventRepository.Add(@event);
            _eventRepository.Save();

            game.EventId = @event.Id;

            game.GameOdds.Single(x => x.Name == "1").Id = @event.Markets.First().Odds.Single(x => x.Name == "1").Id;
            game.GameOdds.Single(x => x.Name == "X").Id = @event.Markets.First().Odds.Single(x => x.Name == "X").Id;
            game.GameOdds.Single(x => x.Name == "2").Id = @event.Markets.First().Odds.Single(x => x.Name == "2").Id;

            //message sending
            var operation = new Operation<Game>(OperationType.Create, game);
            _gameQueue.Send(operation);

            return game.EventId;
        }

        public void DeleteGame(int gameId)
        {
            var @event = _eventRepository.FindBy(x => x.Id == gameId).SingleOrDefault();
            if (@event == null)
                throw new Exception("Event not found.");

            _eventRepository.Delete(@event);
            _eventRepository.Save();

            var operation = new Operation<Game>(OperationType.Delete, new Game() { EventId = gameId });
            _gameQueue.Send(operation);
        }

        public void UpdateGame(Game game)
        {
            var creationTime = DateTime.Now;

            var @event = _eventRepository.FindBy(x => x.Id == game.EventId).SingleOrDefault();
            @event.Name = game.Name;

            foreach (var gameOdd in game.GameOdds)
            {
                @event.Markets.First().Odds
                    .Single(x => x.Id == gameOdd.Id).OddValue
                    .Add(new OddValue()
                    {
                        OddId = gameOdd.Id,
                        CreationTime = creationTime,
                        Value = gameOdd.Value
                    });
            }

            _eventRepository.Edit(@event);
            _eventRepository.Save();

            var operation = new Operation<Game>(OperationType.Update, game);
            _gameQueue.Send(operation);
        }

        public IEnumerable<Game> GetAllGames()
        {
            var marketTypeId_1x2 = _marketTypeRepository.FindBy(x2 => x2.Name == "1X2").First().Id;
            return _eventRepository.GetAll().Select(e => new Game()
             {
                 EventId = e.Id,
                 Name = e.Name,
                 GameOdds = e.Markets.FirstOrDefault(x => x.MarketTypeId == marketTypeId_1x2).Odds.Select(s => new GameOdd()
                 {
                     Id = s.Id,
                     Name = s.Name,
                     Value = s.OddValue.OrderByDescending(x => x.CreationTime).FirstOrDefault().Value, //we'll take the last one inserted, of course.
                 })
             });
        }

        private OddType ConvertNameToOddType(string name)
        {
            switch (name)
            {
                case "1":
                    return OddType.Home;
                case "X":
                case "x":
                    return OddType.Draw;
                case "2":
                    return OddType.Away;
                default:
                    throw new NotImplementedException(name);
                    break;
            }
        }


        public Game Find(int gameId)
        {
            var marketTypeId_1x2 = _marketTypeRepository.FindBy(x2 => x2.Name == "1X2").First().Id;
            return _eventRepository.FindBy(x => x.Id == gameId).Select(e => new Game()
            {
                EventId = e.Id,
                Name = e.Name,
                GameOdds = e.Markets.FirstOrDefault(x => x.MarketTypeId == marketTypeId_1x2).Odds.Select(s => new GameOdd()
                {
                    Id = s.Id,
                    Name = s.Name,
                    Value = s.OddValue.OrderByDescending(x => x.CreationTime).FirstOrDefault().Value, //we'll take the last one inserted, of course.
                })
            }).FirstOrDefault();
        }
    }
}
